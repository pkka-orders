#!/usr/bin/python
# vim: set fileencoding=utf-8 tw=80 :
"""Get last order for PKKA from tumblr using his API"""

# pkka-ordersbot: jabber/xmpp bot for sending orders to PKKA soldiers
# Copyright (c) 2010 Vladimir Jeleznov <megaslow@jabber.ru>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import xml.dom
import xml.dom.minidom
import re
import errno
import threading
import daemon
import daemon.pidlockfile
import time
import logging
import sys
import os
import pwd
import configobj
import urllib2
import argparse
import jabberbot
import html5lib
import cPickle as pickle
from html5lib import treebuilders

class CPSU_bot(jabberbot.JabberBot):
	MSG_AUTHORIZE_ME = 'Hi. Тебя нет в моём ростере. Авторизуй меня, тогда я тоже авторизую тебя.'
	MSG_NOT_AUTHORIZED = 'Ты не авторизовал меня. Доступ запрещён.'

	def get_pkka_subscribelist(self):
		"""Read PKKA subscribers from file"""
		try:
			fp = open(config['pkka_subscribe_list'], 'r')
			self.subs_list = pickle.load(fp)
			fp.close()
		except IOError, e:
			if e.errno == errno.ENOENT:
				self.subs_list = []
			else:
				raise
		return

	def dump_pickle(self, file, obj):
		"""Dump specified object to file"""
		try:
			fp = open(file, 'w')
			pickle.dump(obj, fp)
			fp.close()
		except IOError, e:
			log.error('failed to write pickle to file %s, error: %s', file, e.errno)
			raise
		return

	@jabberbot.botcmd
	def whoami(self, mess, args):
		"""Tells you your jid"""
		return mess.getFrom().getStripped()

	@jabberbot.botcmd
	def subscribe(self, mess, args):
		"""Subscribe to PKKA orders"""
		jid = mess.getFrom().getStripped()
		if not jid in self.subs_list:
			self.subs_list.append(jid)
			self.dump_pickle(config['pkka_subscribe_list'], self.subs_list)
			msg = 'jid ' + jid + ' successfully subscribed to PKKA orders.'
			log.debug('subscribed to orders: %s', jid)
		else:
			msg = 'jid ' + jid + ' already in subscribe list.'
		return msg

	@jabberbot.botcmd
	def unsubscribe(self, mess, args):
		"""Unsubscribe from PKKA orders"""
		jid = mess.getFrom().getStripped()
		if jid in self.subs_list:
			self.subs_list.remove(jid)
			self.dump_pickle(config['pkka_subscribe_list'], self.subs_list)
			msg = 'jid ' + jid + ' successfully unsubscribed from PKKA orders.'
			log.debug('unsubscribed from PKKA orders: %s', jid)
		else:
			msg = 'jid ' + jid + ' wasn\'t subscribed.'
		return msg

	# FIXME(jeleznov): need to add DisconnectHandler (see xmpp/client.py from
	# python module xmpppy)

class ParseTumblr(threading.Thread):
	"""Parse order from tumblr"""

	def __init__(self):
		threading.Thread.__init__(self)
		self.setDaemon(True)
		self.active = True

	def get_order_id(self, data):
		"""Get order id from special-formed comment"""
		m = re.search(r'order_id=(\d+)', data)
		if m == None:
			return 0
		else:
			return m.group(1)

	def remove_html_tags(self, data):
		"""Remove all HTML tags except <a>"""
		p = re.compile('<(?!\/?a(?=>|\s.*>))\/?.*?>')
		return p.sub('', data)

	def sanitize_urls(self, data):
		"""Get content inside <a></a> tags and "href" attribute"""
		patt=r'<a.+?href="(http[^"]+)"(?:>|\s.*?>)([^<]+)</a>'
		repl=r'\2: \1 '
		return re.sub(patt, repl, data)

	def run(self):
		while self.active:
			log.debug('starting update iteration')
			# form doc object by parsing url
			try:
				response = urllib2.urlopen(config['url'])
			except urllib2.URLError, e:
				if hasattr(e, 'reason'):
					log.warning('failed to reach server, reason: %s', e.reason)
				elif hasattr(e, 'code'):
					log.warning('server can\'t finish the request, error code: %s', e.code)
			else:
				data = response.read().decode('utf-8')
				doc = html5lib.HTMLParser(tree=treebuilders.getTreeBuilder("dom")).parse(data)

				# check for 'div' tag existence
				try:
					div = doc.getElementsByTagName('div')
				except IndexError:
					last_order_id = 0

				for node in doc.getElementsByTagName('div'):
					if node.getAttribute('class') == 'regular':
						# dirty body, with html tags
						dirty_body = node.toxml()

						# latest order id
						last_order_id = int(self.get_order_id(dirty_body))

				log.debug('last_order_id: %s', last_order_id)

				# read old order id from file
				try:
					f = open(config['stamp_file'], 'r')
					old_order_id = int(f.read())
					f.close()
				except IOError, e:
					if e.errno == errno.ENOENT:
						old_order_id = 0
					else:
						raise

				# send message only if last post id is changed
				if old_order_id < last_order_id and last_order_id != 0:
					# write last order id to file
					try:
						f = open(config['stamp_file'], 'w')
						f.write(str(last_order_id))
						f.close()
						log.debug('successfully wrote stamp_file: %s', config['stamp_file'])
					except IOError, e:
						log.error('failed to write stamp_file %s, error: %s',
								config['stamp_file'], e.errno)
						raise

					# body with stripped tags except <a>
					body_with_a = self.remove_html_tags(dirty_body)

					# awesome body with URLs from <a> tags
					body = self.sanitize_urls(body_with_a)

					# send body to subscribers
					log.debug('id changed, old: %s, new: %s', old_order_id, last_order_id)
					for subscriber in bot.subs_list:
						log.debug('sending order to: %s', subscriber)
						bot.send(subscriber, body, message_type='')
						time.sleep(0.2)
			time.sleep(float(config['interval']))

def main():
	"""Launch jabber bot and tumbler updater."""
	log.debug('main() started')

	updater = ParseTumblr()
	updater.start()

	global bot
	bot = CPSU_bot(config['username'], config['password'])
	bot.get_pkka_subscribelist()
	bot.serve_forever()

# create logger
log = logging.getLogger('PKKA-bot')
LEVELS = {
	'debug': logging.DEBUG,
	'info': logging.INFO,
	'warning': logging.WARNING,
	'error': logging.ERROR,
	'critical': logging.CRITICAL
}

# parse command line options
parser = argparse.ArgumentParser(description='Jabber bot for PKKA.')
parser.add_argument('-f', '--foreground', action='store_true', help='do not detach from terminal')
parser.add_argument('-c', '--config', default='/etc/pkka-ordersbot.conf', dest='config_file',
		help='use specified config instead of default')
args = parser.parse_args()

# Get config
config = configobj.ConfigObj(args.config_file)
if not config:
	sys.stderr.write("Can't read config from %s file\n" % args.config_file)
	sys.exit(1)

# get pid object for daemon
pid = daemon.pidlockfile.TimeoutPIDLockFile(config['pidfile'], 10)

# create daemon context
dmn = daemon.DaemonContext()
dmn.pidfile = pid

# define umask for daemon
dmn.umask = 007

# check for --foreground
if args.foreground:
	# log to console in this mode
	handler = logging.StreamHandler()
	log.setLevel(logging.DEBUG)

	dmn.detach_process = False
	dmn.stdout = sys.stdout
	dmn.stderr = sys.stderr
else:
	handler = logging.FileHandler(config['LOG_FILENAME'])
	level = LEVELS.get(config['log_level'], logging.NOTSET)
	log.setLevel(level)
	dmn.files_preserve=[handler.stream]

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)

# define uid and gid for daemon
if os.getuid() == 0:
	pw_entry = pwd.getpwnam(config['runas_user'])
	log.debug('dropping privileges to: %s', config['runas_user'])
	dmn.uid = pw_entry[2]
	dmn.gid = pw_entry[3]

dmn.open() # become daemon
main()
